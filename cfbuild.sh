#!/bin/sh
curl -sSL https://dot.net/v1/dotnet-install.sh > dotnet-install.sh
chmod +x dotnet-install.sh
./dotnet-install.sh -c 6.0 -InstallDir ./dotnet6
./dotnet6/dotnet tool restore
npm run cf-prod
npm run cf-babel
cp -R yachtuwp/* yachtserver/WebRoot/
rm -rf yachtserver/WebRoot/lib
rm -rf yachtserver/WebRoot/images
rm yachtserver/WebRoot/index.html
mv yachtserver/WebRoot/app.html yachtserver/WebRoot/index.html
rm yachtserver/WebRoot/package.appxmanifest
rm yachtserver/WebRoot/yachtuwp.jsproj


