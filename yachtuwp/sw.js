﻿const CACHE_NAME = 'static-cache-v13';

const FILES_TO_CACHE = [
	'/manifest.json',
	'/app.html',
	'/css/style.css',
	'/icon.ico',
	'/apple-touch-icon.png',
	'/js/startup.js',
	'/js/bundle.js',
	'/c/c0.svg',
	'/c/c1.svg',
	'/c/c2.svg',
	'/c/c3.svg',
	'/c/c4.svg',
	'/c/c5.svg',
	'/c/c6.svg',
	'/img/icon128x128.png',
	'/img/icon144x144.png',
	'/img/icon152x152.png',
	'/img/icon192x192.png',
	'/img/icon256x256.png',
	'/img/icon512x512.png',
	'/img/wood.svg',
	'/img/edit.svg',
	'/img/rotate-cw.svg',
	'/img/x-circle.svg',
	'/img/menu.svg'
];

self.addEventListener('install', (e) => 
{
	//console.log('[ServiceWorker] Install');
	e.waitUntil(
	    caches.open(CACHE_NAME).then((cache) => {
	      //console.log('[ServiceWorker] Pre-caching offline page');
	      return cache.addAll(FILES_TO_CACHE);
	    })
	);
});

self.addEventListener('activate', (e) =>
{
	e.waitUntil(
	    caches.keys().then((keyList) => {
	      return Promise.all(keyList.map((key) => {
	        if (key !== CACHE_NAME) {
	          //console.log('[ServiceWorker] Removing old cache', key);
	          return caches.delete(key);
	        }
	      }));
	    })
	);
});
self.addEventListener('fetch', (e) => {
//  console.log('[ServiceWorker] Fetch', e.request.url);
	e.respondWith(
	    caches
	    	.open(CACHE_NAME)
	    	.then((cache) => 
	    	{
	    		return cache
	    			.match(e.request)
	    			.then((response) =>
					{
						return response || fetch(e.request);
					});
	        })
	);
});