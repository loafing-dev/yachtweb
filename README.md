# Modern Yacht Web brings a classic dice game to all of your devices

Modern Yacht Web is built around the BSD-licensed ModernYachtCore Nuget Package.

## App
The "App" project is a Fable .NET Standard library that forms the backbone of both a PWA
and the WinJS project

## YachtServer
Locally, YachtServer provides the API for multiplayer games, third-party clients, and a static experience for older devices.
When this project is built using the pipeline, it also bundles the PWA with it.

## YachtUWP
This WinJS project contains the majority of the PWA code (except a custom index.html and a few JS files). The WinJS project is
primarily targeted at Xbox and Windows 10 Mobile, though is also the first release for Desktop.