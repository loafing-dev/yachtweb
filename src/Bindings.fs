﻿module Bindings

open Model
open Browser


let form = document.querySelector("form") :?> Browser.Types.HTMLFormElement

form.addEventListener("submit", (fun e -> e.preventDefault()))


let dice : DieRecord list = [
    { 
        Image = document.querySelector("#die1-img") :?> Browser.Types.HTMLImageElement;
        Checkbox = document.querySelector("#die1") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Image = document.querySelector("#die2-img") :?> Browser.Types.HTMLImageElement;
        Checkbox = document.querySelector("#die2") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Image = document.querySelector("#die3-img") :?> Browser.Types.HTMLImageElement;
        Checkbox = document.querySelector("#die3") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Image = document.querySelector("#die4-img") :?> Browser.Types.HTMLImageElement;
        Checkbox = document.querySelector("#die4") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Image = document.querySelector("#die5-img") :?> Browser.Types.HTMLImageElement;
        Checkbox = document.querySelector("#die5") :?> Browser.Types.HTMLInputElement;
    };
]

let rows : RowRecord list = [
    { 
        Title = document.querySelector("#t01") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o01") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r01") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#ones") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t02") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o02") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r02") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#twos") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t03") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o03") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r03") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#threes") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t04") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o04") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r04") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#fours") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t05") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o05") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r05") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#fives") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t06") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o06") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r06") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#sixes") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t08") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o08") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r08") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#threekind") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t09") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o09") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r09") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#fourkind") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t10") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o10") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r10") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#fullhouse") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t11") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o11") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r11") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#smallstraight") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t12") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o12") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r12") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#largestraight") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t13") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o13") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r13") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#yacht") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t14") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o14") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r14") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#chance") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t15") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o15") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r15") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#bonus") :?> Browser.Types.HTMLInputElement;
    };
    { 
        Title = document.querySelector("#t07") :?> Browser.Types.HTMLSpanElement;
        Span  = document.querySelector("#o07") :?> Browser.Types.HTMLSpanElement;
        Label = document.querySelector("#r07") :?> Browser.Types.HTMLLabelElement;
        Radio = document.querySelector("#upperbonus") :?> Browser.Types.HTMLInputElement;
    };
]

let rollbutton = document.querySelector("#go") :?> Browser.Types.HTMLButtonElement
let placebtn = document.querySelector("#placebutton") :?> Browser.Types.HTMLButtonElement
let restartbtn = document.querySelector("#restart") :?> Browser.Types.HTMLButtonElement
let menubtn = document.querySelector("#menubtn") :?> Browser.Types.HTMLButtonElement
let menuclose = document.querySelector("#menuclose") :?> Browser.Types.HTMLButtonElement
let menucontents = document.querySelector("#menucontents") :?> Browser.Types.HTMLDivElement
let menutitle = document.querySelector("#menutitle") :?> Browser.Types.HTMLDivElement
let menu = document.querySelector("#menu") :?> Browser.Types.HTMLDivElement
let placeout = document.querySelector("#selectedrow") :?> Browser.Types.HTMLSpanElement
let scoreout = document.querySelector("#score") :?> Browser.Types.HTMLSpanElement