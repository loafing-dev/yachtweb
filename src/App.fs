module App

open Browser.Dom
open ModernYachtCore.Board
open ModernYachtCore.Model
open Fable.Core.JsInterop
open Bindings
open State
open Utility
open Model

if not (window?Windows = null)
then printf "Windows"
else printf "Not Windows!"

//doesn't seem to matter for non-WinJS, enables WinJS
window?navigator?gamepadInputEmulation <- "gamepad"; 

let rollfn (reset: bool) =
    getholds 0

    let newdice = 
        if reset 
        then
            [ 0; 0; 0; 0; 0; ]
        else
            List.map2 
                rollDie
                (Seq.toList board.dice)
                (Seq.toList board.hold)
            
    roll <- if roll = 3 || reset then 0 else roll + 1
    board <- 
        {   
            roll = board.roll
            dice = newdice
            hold = 
                if reset 
                then seq { false; false; false; false; false; } 
                else board.hold
            playerid = board.playerid
            playercount = board.playercount
        }
    displayDie board.dice board.hold
    controlsupdate roll

rollbutton.onclick <- (fun _ -> 
    if not (roll = 3) && (Seq.contains -1 player.scores) then (rollfn false))

let rollcomplete (result: int seq) : unit =
    displayScores result
    player <- { name = player.name; scores = result; }
    place <- -1
    rollfn true

let placejoker (dice : int seq) (result : int seq) (row : int): (Browser.Types.MouseEvent -> unit) = 
    let complete _ =
        if Seq.item row result = -1
        then
            let (joker, _) = PlaceJoker dice result row
            rollcomplete joker
            toggleMenu 0
            menuclose.disabled <- false
    complete

placebtn.onclick <- fun e ->
    getplace 0
    if not (roll = 0) && place > -1
    then
        let (result, error) = 
            Eval 
                board.dice 
                player.scores
                place 
        if error = -5
        then
            printfn "Out of range row! Pick between 0 and 13"
            window.alert "Out of range row! Pick between 0 and 13"
        elif error = 0
        then
            rollcomplete result
        elif error = -1
        then 
            printfn "That row is full! Try again"
            window.alert "That row is full! Try again"
        elif error = -2
        then 
            printfn "Yahtzee!"
            rollcomplete result
        elif error = -3
        then 
            printfn "Yahtzee Bonus! Your Upper Table row has also been filled."
            rollcomplete result
        elif error = -4
        then 
            printfn "Yahtzee Bonus! Your Upper Table row is already filled."
            printfn "Pick a Lower Table row to use your Joker!"
            
            //modal for placing it
            let rows : MenuRowData list = 
                [
                    if Seq.item 6 result = -1 
                    then 
                        yield { 
                            Title = "Three of a Kind"; 
                            Click = placejoker (Seq.toList board.dice) result 6; 
                        };
                    if Seq.item 7 result = -1 
                    then
                        yield { 
                            Title = "Four of a Kind"
                            Click = placejoker (Seq.toList board.dice) result 7;
                        };
                    if Seq.item 8 result = -1 
                    then
                        yield { 
                            Title = "Full House"; 
                            Click = placejoker (Seq.toList board.dice) result 8; 
                        };
                    if Seq.item 9 result = -1 
                    then
                        yield { 
                            Title = "Small Straight"
                            Click = placejoker (Seq.toList board.dice) result 9;
                        };
                    if Seq.item 10 result = -1 
                    then
                        yield { 
                            Title = "Large Straight"; 
                            Click = placejoker (Seq.toList board.dice) result 10; 
                        };
                    if Seq.item 12 result = -1 
                    then
                        yield { 
                            Title = "Chance"
                            Click = placejoker (Seq.toList board.dice) result 12;
                        };
                ]
            setMenuContents "Yacht Joker!" rows
            menuclose.disabled <- true
            toggleMenu 0

        else rollcomplete result
            

restartbtn.onclick <- fun _ ->
    loadBoard DefaultBoard
    loadPlayer { name = "Default"; scores = EmptyTable }

menubtn.onclick <- fun _ ->
    let rows : MenuRowData list = 
        [
            { 
                Title = "New Game"
                Click = 
                (fun _ 
                    -> 
                        loadBoard DefaultBoard
                        loadPlayer { name = "Default"; scores = EmptyTable }
                        toggleMenu 0
                )    
            };
            { 
                Title = "Load Game"
                Click = 
                (fun _ 
                    -> 
                        loadBoard 
                            {
                                dice = seq { 1; 1; 1; 1; 1; }
                                hold = seq { false; false; false; false; false; }
                                roll = 1
                                playerid = 1
                                playercount = 1
                            }
                        loadPlayer { name = "Default"; scores = seq {2; -1; -1; -1; -1; -1; -1; -1; -1; -1; 40; 50; -1; 100; 0} }
                        toggleMenu 0
                )
            };
            { 
                Title = "Save Game"
                Click = 
                (fun _ 
                    -> 
                        toggleMenu 0
                )
            };
            { 
                Title = "View High Score"
                Click = (fun _ -> setMenuHighScore { name = "Default"; scores = EmptyTable })
            };
            { 
                Title = "Create Multiplayer"
                Click = 
                (fun _ 
                    -> 
                        toggleMenu 0
                )
            };
            { 
                Title = "Join Multiplayer"
                Click = 
                (fun _ 
                    -> 
                        toggleMenu 0
                )
            };
        ]
    setMenuContents "Menu" rows
    toggleMenu 0

menuclose.onclick <- fun _ ->
    toggleMenu 0